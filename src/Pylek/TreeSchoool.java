package Pylek;

import java.util.HashMap;

public class TreeSchoool {
    HashMap<String, Tree> trees = new HashMap<>();

    public Tree getForestTree(String kind) {
        Tree forestTree = trees.get(kind);
        System.out.println("start sadzenia drzwwa");
        if (forestTree == null) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            forestTree = new ForestTree(kind);
            trees.put(kind, forestTree);
        }
        System.out.println("koniec sadzenia drzewa");
        return forestTree;
    }
}
