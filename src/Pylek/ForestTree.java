package Pylek;

public class ForestTree implements Tree {

    String kindOfTree;

    public ForestTree(String kindOfTree) {
        this.kindOfTree = kindOfTree;
    }

    @Override
        public void plantTree() {
        System.out.println("Sadzimy drzewo odmiany " + this.kindOfTree);
    }
}
