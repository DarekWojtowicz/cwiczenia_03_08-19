package Pylek;

public class Main {

    public static void main(String[] args) {

        TreeSchoool treeSchoool = new TreeSchoool();
        Tree oak1 = treeSchoool.getForestTree("Oak");
        Tree ash1 = treeSchoool.getForestTree("Ash");
        Tree oak2 = treeSchoool.getForestTree("Oak");
        Tree ash2 = treeSchoool.getForestTree("Ash");
        Tree linden1 = treeSchoool.getForestTree("Linden");

        oak1.plantTree();
        System.out.println(oak1.hashCode());

        oak2.plantTree();
        System.out.println(oak2.hashCode());

        ash1.plantTree();
        System.out.println(ash1.hashCode());

        ash2.plantTree();
        System.out.println(ash2.hashCode());

        linden1.plantTree();
        System.out.println(linden1.hashCode());
    }
}
