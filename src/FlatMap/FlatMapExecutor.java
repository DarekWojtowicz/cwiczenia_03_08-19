package FlatMap;

import javax.xml.transform.sax.SAXSource;
import java.util.Arrays;
import java.util.List;

public class FlatMapExecutor {
    enum VideoType {
        CLIP, PREVIEW, EPISODE
    }

    static class Video {
        public String title;
        public String url;
        public VideoType videoType;

        public Video(String title, String url, VideoType videoType) {
            this.title = title;
            this.url = url;
            this.videoType = videoType;
        }

        @Override
        public String toString() {
            return "Video{" +
                    "title='" + title + '\'' +
                    ", url='" + url + '\'' +
                    ", videoType=" + videoType +
                    '}';
        }
    }

    static class Episode {
        public String episodeName;
        public int episodeNumber;
        List<Video> videos;

        public Episode(String episodeName, int episodeNumber, List<Video> videos) {
            this.episodeName = episodeName;
            this.episodeNumber = episodeNumber;
            this.videos = videos;
        }

        @Override
        public String toString() {
            return "Episode{" +
                    "episodeName='" + episodeName + '\'' +
                    ", episodeNumber=" + episodeNumber +
                    ", videos=" + videos +
                    '}';
        }
    }

    static class Season {
        public String seasonName;
        public int seasonNumber;
        List<Episode> episodes;

        public Season(String seasonName, int seasonNumber, List<Episode> episodes) {
            this.seasonName = seasonName;
            this.seasonNumber = seasonNumber;
            this.episodes = episodes;
        }

        @Override
        public String toString() {
            return "Season{" +
                    "seasonName='" + seasonName + '\'' +
                    ", seasonNumber=" + seasonNumber +
                    ", episodes=" + episodes +
                    '}';
        }
    }

    public static void main(String[] args) {

        Video video = new Video("GOT1", "got1.com", VideoType.CLIP);
        Video video1 = new Video("GOT2", "got2.com", VideoType.EPISODE);
        Video video2 = new Video("GOT3", "got3.com", VideoType.PREVIEW);
        Video video3 = new Video("GOT4", "got4.com", VideoType.PREVIEW);
        Video video4 = new Video("GOT5", "got5.com", VideoType.CLIP);
        Video video5 = new Video("GOT6", "got6.com", VideoType.EPISODE);

        Episode episode1 = new Episode("got1", 1,
                Arrays.asList(video, video1));
        Episode episode2 = new Episode("got2", 2,
                Arrays.asList(video2, video3));
        Episode episode3 = new Episode("got3", 3,
                Arrays.asList(video4, video5));

        Season season1 = new Season("GOTS1", 1,
                Arrays.asList(episode1, episode2));

        Season season2 = new Season("GOTS2", 2,
                Arrays.asList(episode3));

        List<Season> seasons = Arrays.asList(season1, season2);


        //1. Zwróć listę wszystkich episodów
        System.out.println("\n1. Zwróć listę wszystkich episodów\n");
        seasons.stream()
                .flatMap((Season s) -> {

                    return s.episodes.stream();
                })
                .forEach((Episode e) -> {
                    System.out.println(e);
                });

        //2. Zwróć listę wszystkich filmów
        System.out.println("\n2. Zwróć listę wszystkich filmów\n");
        seasons.stream()
                .flatMap((Season s) -> {
                    return s.episodes.stream();
                })
                .flatMap((Episode e) -> {
                    return e.videos.stream();
                })
                .forEach((Video v) -> {
                    System.out.println(v);
                });

        //3. Zwróć listę wszystkich nazw sezonów
        System.out.println("\n3. Zwróć listę wszystkich nazw sezonów\n");
        seasons.stream()
                .map((Season s) -> {
                    return s.seasonName;
                })
                .forEach((String s) -> {
                    System.out.println(s);
                });

        // 4. Zwróć listę wszystkich numerów sezonów
        System.out.println("\n4. Zwróć listę wszystkich numerów sezonów\n");
        seasons.stream()
                .map((Season s) -> {
                    return s.seasonNumber;
                })
                .forEach((Integer i) -> {
                    System.out.println(i);
                });

        //5. Zwróć listę wszystkich nazw episodów
        System.out.println("\n5. Zwróć listę wszystkich nazw episodów\n");
        seasons.stream()
                .flatMap((Season s) -> {
                    return s.episodes.stream();
                })
                .map((Episode e) -> {
                    return e.episodeName;
                })
                .forEach((String s) -> {
                    System.out.println(s);
                });

        //6. Zwróć listę wszystkich numerów episodów
        System.out.println("\n6. Zwróć listę wszystkich numerów episodów\n");
        seasons.stream()
                .flatMap((Season s) -> {
                    return s.episodes.stream();
                })
                .map((Episode e) -> {
                    return e.episodeNumber;
                })
                .forEach((Integer i) -> {
                    System.out.println(i);
                });

        //7. Zwróć listę wszystkich nazw video
        System.out.println("\n7. Zwróć listę wszystkich nazw video\n");
        seasons.stream()
                .flatMap((Season s) -> {
                    return s.episodes.stream();
                })
                .flatMap((Episode e) -> {
                    return e.videos.stream();
                })
                .map((Video v) -> {
                    return v.title;
                })
                .forEach((String s) -> {
                    System.out.println(s);
                });

        //8. Zwróć listę wszystkich numerów video
        System.out.println("\n8. Zwróć listę wszystkich url video\n");
        seasons.stream()
                .flatMap((Season s) -> {
                    return s.episodes.stream();
                })
                .flatMap((Episode e) -> {
                    return e.videos.stream();
                })
                .map((Video v) -> {
                    return v.url;
                })
                .forEach((String s) -> {
                    System.out.println(s);
                });

        //druga część

        //1. Wyświetl tylko epiosdy z parzystych sezonów
        System.out.println("\n1. Wyświetl tylko epiosdy z parzystych sezonów\n");
        seasons.stream()
                .filter((Season s) -> {
                    return s.seasonNumber % 2 == 0;
                })
                .flatMap((Season s) -> {
                    return s.episodes.stream();
                })
                .forEach((Episode e) -> {
                    System.out.println(e);
                });

        //2. Wyświetl tylko video z parzystych sezonów
        System.out.println("\n2. Wyświetl tylko video z parzystych sezonów\n");
        seasons.stream()
                .filter((Season s) -> {
                    return s.seasonNumber % 2 == 0;
                })
                .flatMap((Season s) -> {
                    return s.episodes.stream();
                })
                .flatMap((Episode e) -> {
                    return  e.videos.stream();
                })
                .forEach(v -> {
                    System.out.println(v);
                });

        //3. Wyświetl tylko video z parzystych episody i sezonów
        System.out.println("\n3. Wyświetl tylko video z parzystych episody i sezonów\n");
        seasons.stream()
                .filter((Season s) -> {
                    return s.seasonNumber % 2 == 0;
                })
                .flatMap((Season s) -> {
                    return s.episodes.stream();
                })
                .filter((Episode e) -> {
                    return e.episodeNumber % 2  == 0;
                })
                .flatMap((Episode e) -> {
                    return e.videos.stream();
                })
                .forEach((Video v) -> {
                    System.out.println(v);
                });
    }
}
